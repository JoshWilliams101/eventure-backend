


### This repository is for setting up the express REST API for eventure ###
#### If you're looking for the frontend repo go [here](https://bitbucket.org/JoshWilliams101/eventure-frontend/admin) ####
### How do I get set up? ###

* First you will need to install [Node.js](https://nodejs.org/en/)
* After installing Node, you should have npm installed already, which is the node.js package manager.


### Dependencies ###
* Open CLI and `cd project_directory` then Run `npm install` to install all dependencies
### Database configuration ###
* MySQL must be installed, and import the "eventure_db.sql" file into your database. *It has the create database sql statement already*.

### Deployment instructions ###
* To start the server simply run `node bin/www`. If you want to run the server in the background rerun if a change is made, install forever by runnin `npm install -g forever`. Then run the project by executing `forever start bin/www`

Folder/Filename Location | Functionality
--- | ---
** /bin/ ** | contains the script that starts the server, along with the socket.io portion
** /helpers/middleware.js ** | contains middleware that will be attached to certain routes such as middleware to check if a user is logged in before they could access a route.
** /models/ ** | contains all of the Bookshelf.js models 
** /passport/ ** | contains code for authentication and session handling
** /routes/index.js ** | file that handles routes for authentication, and logging out.
** /routes/api.js && routes/apiRoutes/* ** | files that wire up all endpoints to handle all http requests respective to their name
**/app.js** | sets up most of the applications variables and dependencies.
** /config.js/ ** | configuration file for database connection and passport secrets( i shouldn't be showing this but whatever)
** /database.js ** | establish connection to database with bookshelf ORM
** /postman_routes.json ** | you can import this to postman if you plan on testing the api using it. In order to use postman you need to add the origin of the postman client to the whitelist on line 34 and in the accepted_domains array in the middleware.js file. In order to find out what the origin of your postman client is, you can add a `console.log("\n\norigin is: ", origin)` right above `var originIsWhitelisted  = !orig...`. Then run the server using `node bin/www` and make any request using postman to the server. In the command line you will see what the origin is. Copy and paste it and add it to the arrays listed above.





### Who do I talk to? ###
email me @ jwilliams199055@gmail.com for any inquiries